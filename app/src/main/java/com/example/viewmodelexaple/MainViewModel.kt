package com.example.viewmodelexaple


import androidx.lifecycle.ViewModel

class MainViewModel : ViewModel() {

    var cont : Int = 0

    fun increment(){
        cont ++
    }
}