package com.example.viewmodelexaple

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.lifecycle.ViewModelProvider

class MainActivity : AppCompatActivity() {

    var cont : Int = 0
    lateinit var txtCountr : TextView
    lateinit var mainViewModel: MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mainViewModel = ViewModelProvider(this ).get(MainViewModel :: class.java)
        txtCountr = findViewById(R.id.txtContr)
        setText()
    }

    fun increment(v : View){
        mainViewModel.increment()
        setText()
    }

    private fun setText() {
      txtCountr.text = mainViewModel.cont.toString()
    }
}